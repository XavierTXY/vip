//
//  Constant.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 21/8/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import Foundation
import UIKit

let BORDER_COLOR = UIColor(red: 226/255, green: 228/255, blue: 232/255, alpha: 1)

let NO_PICTURE = 10

let NO_PIC = "NoPicture"

let CRUSHER = "Crusher"

let MIXER = "Mixer"

let HOPPER = "Hopper"

let AUTOLOADER = "Autoloader"

let CHILLER = "Chiller"

let MTC = "MTC"

let MAIN_COLOR = UIColor(red: 18/255, green: 109/255, blue: 215/255, alpha: 1)

let PURPLE = UIColor(red: 18/255, green: 102/255, blue: 215/255, alpha: 1)

let ADMIN_ID = "z6ZQVUAh7kfZ5jxyPGjEMptCo7J3"
