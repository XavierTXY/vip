//
//  FirebaseService.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 20/8/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import SVProgressHUD

class FirebaseService {
    
    static let fb = FirebaseService()
    
    
    func uploadData(itemValue: Dictionary<String, AnyObject>,images: [UIImage], vc: UIViewController, ref: DatabaseReference?) {
        SVProgressHUD.show()
        var firebasePost: DatabaseReference!
        
        if ref != nil {
            ref!.setValue(itemValue)
            
            DataService.ds.REF_Category.child("\(itemValue["itemCat"]!)").child(ref!.key).setValue(itemValue)
            uploadImg(images: images,id: ref!.key, vc: vc, cat: itemValue["itemCat"]! as! String)
        } else {
            firebasePost = DataService.ds.REF_Item.childByAutoId()
            
            firebasePost.setValue(itemValue)
            
            DataService.ds.REF_Category.child("\(itemValue["itemCat"]!)").child(firebasePost.key).setValue(itemValue)
            uploadImg(images: images,id: firebasePost.key, vc: vc, cat: itemValue["itemCat"]! as! String)
        }

        
//        DataService.ds.REF_Category.child("\(itemValue["itemCat"]!)").child(firebasePost.key).setValue(itemValue)
//        uploadImg(images: images,id: firebasePost.key, vc: vc, cat: itemValue["itemCat"]! as! String)
    }
    
    func uploadImg(images: [UIImage], id: String,vc: UIViewController, cat: String) {
        
        var numPhoto = 0
        var idx = 0
        
        var urlDict = Dictionary<String, Bool>()
        for img in images {
            if img != #imageLiteral(resourceName: "vipLogo") {
                numPhoto = numPhoto + 1
                if let imgData = UIImageJPEGRepresentation(img, 0.1) {
                    
                    let imgUid = NSUUID().uuidString
                    let metaData = StorageMetadata()
                    metaData.contentType = "image/jpeg"
                    
                    DataService.ds.REF_Item_Pic.child(imgUid).putData(imgData, metadata: metaData) { (metaData, error) in
                        if error != nil {
                            print("Xavier: upload image to firebase failed")
                            print(error.debugDescription)
                        } else {
                            print("Xavier: successfuly upload img to firebase")
                            let downloadUrl = metaData?.downloadURL()?.absoluteString
                            if let url = downloadUrl {
//                                self.postToFirebase(imgUrl: url)
                                
                                var updateObj = [String:AnyObject]()
                                let ref = DB_BASE
                                updateObj["/item/\(id)/urls/url\(idx)"] = url as AnyObject
                                updateObj["/category/\(cat)/\(id)/urls/url\(idx)"] = url as AnyObject
                                idx = idx + 1
                                ref.updateChildValues(updateObj)
                                
                          
                                if idx == numPhoto {
                                    SVProgressHUD.showSuccess(withStatus: "Done!")
                                    vc.dismiss(animated: true, completion: nil)
                                }

                                
                            }
                            
                        }
                        
                    }
                }
            }
        }



    }

}
