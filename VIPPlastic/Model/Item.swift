//
//  Item.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 23/8/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import Foundation
import Firebase

class Item {
    var itemKey: String!
    private var _category: String!
    private var _condition: String!
    private var _description: String!
    private var _name: String!
    private var _brand: String!
    private var _price: String!
    private var _urls: [String: String]!
    private var _types: [String: Bool]!
    private var _type: String!
    private var _url: String!
    var typeArray = [String]()
    var unit: Int!
    
    var itemRef: DatabaseReference!
    
    var category: String? {
        return _category
    }
    
    var brand: String? {
        return _brand
    }
    
    var condition: String? {
        return _condition
    }
    
    var description: String? {
        return _description
    }
    
    var name: String? {
        return _name
    }
    
    var price: String? {
        return _price
    }
    
    var type: String? {
        return _type
    }
    
    var url: String? {
        return _url
    }
    
//    var unit: Int? {
//        set(newUnit) {
//            self._unit = newUnit
//        }
//        
//        get {
//            return _unit
//        }
//        
//    }
    
    var urls: [String: String] {
        return _urls
    }
    
    var types: [String: Bool] {
        return _types
    }
    
    init(name:String, url:String, unit: Int, type: String) {
        
//        if let name = itemData["itemName"] as? String{
            self._name = name
//        }
        
//        if let type = itemData["itemType"] as? String {
            self._type = type
//        }
        
//        if let unit = itemData["itemUnit"] as? String {
            self.unit = unit
//        }
        
//        if let url = itemData["itemUrl"] as? String {
            self._url = url
//        }
        
        
    }
    
    init(itemKey: String, itemData: Dictionary<String, AnyObject>) {

        self.itemKey = itemKey
        
        itemRef = DataService.ds.REF_Item.child(itemKey)
        
        if let category = itemData["itemCat"] as? String{
            self._category = category
        }
        
        if let con = itemData["itemCon"] as? String {
            self._condition = con
        }
        
        if let desc = itemData["itemDesc"] as? String {
            self._description = desc
        }
        
        if let name = itemData["itemName"] as? String {
            self._name = name
        }
        
        if let brand = itemData["itemBrand"] as? String {
            self._brand = brand
        }
        
        if let price = itemData["itemPrice"] as? String {
            self._price = price
        }
        
        if let urls = itemData["urls"] as? [String: String] {
            self._urls = urls
        }
        
        if let types = itemData["itemTypes"] as? [String: Bool] {
   
//            print("NOCE \(types.sorted(by: { $0.0 < $1.0 }))")
            let a = types.sorted(by: { $0.0 < $1.0 })
            var newDict = [String:Bool]()
            for dict in a {
                typeArray.append(dict.key)
//                newDict[dict.key] = dict.value
//                print(dict.key)
            }
            
            //fix here
            self._types = newDict
            
        }
    }
    
    func updateData() {
        
    }
}
