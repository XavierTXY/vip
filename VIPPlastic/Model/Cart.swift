//
//  Cart.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 27/9/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import Foundation


class Cart {
    
    static let shoppingCart = Cart()
    var cart = [Item]()
    var cartDict = [String:Item]()
    
    var itemName: String!
    var itemUrl: String!
    var itemType: String!
    var itemUnit: String!
    
    init() {
        
    }
    
    func removeAll() {
        var updateObj = [String:AnyObject]()
        let ref = DataService.ds.REF_CART_USER
        updateObj["/\(DataService.ds.currentUser.userKey!)"] = NSNull()
        ref.updateChildValues(updateObj)
        
        cartDict.removeAll()
        cart.removeAll()
    }
    
    func updateCart(name:String, url:String, unit: Int, type: String, add: Bool)  {
        
        var updateObj = [String:AnyObject]()
        
        if add {
            let ref = DataService.ds.REF_CART_USER.child(DataService.ds.currentUser.userKey!).child(type)
            
            if cartDict[name] != nil {
                
                var storedItem = cartDict[name]
                var currentUnit = storedItem?.unit
                
                if storedItem?.type! == type {
                    storedItem!.unit = unit + currentUnit!
                    
                    
                    updateObj["/itemName"] = name as AnyObject
                    updateObj["/itemUrl"] = url as AnyObject
                    updateObj["/itemType"] = type as AnyObject
                    updateObj["/itemUnit"] = storedItem?.unit as AnyObject
                    
                    
                    
                    var index = 0
                    for item in cart {
                        if item.name! == storedItem?.name && item.type! == storedItem?.type {
                            cart.remove(at: index)
                            index = index + 1
                        }
                    }
                    
                    cartDict[name] = storedItem
                    cart.append(storedItem!)
                    
                    ref.updateChildValues(updateObj)
                    
                    
                } else {
                    updateObj["/itemName"] = name as AnyObject
                    updateObj["/itemUrl"] = url as AnyObject
                    updateObj["/itemType"] = type as AnyObject
                    updateObj["/itemUnit"] = unit as AnyObject
                    
                    
                    
                    
                    let item = Item(name: name, url: url, unit: unit, type: type)
                    cartDict[name] = storedItem
                    cart.append(item)
                    ref.updateChildValues(updateObj)
                }
            } else {
                
                
                updateObj["/itemName"] = name as AnyObject
                updateObj["/itemUrl"] = url as AnyObject
                updateObj["/itemType"] = type as AnyObject
                updateObj["/itemUnit"] = unit as AnyObject
                
                
                
                
                let item = Item(name: name, url: url, unit: unit, type: type)
                cartDict[name] = item
                cart.append(item)
                ref.updateChildValues(updateObj)
            }

            
        } else {
            let ref = DataService.ds.REF_CART_USER.child(DataService.ds.currentUser.userKey!).child(type)
            
            var storedItem = cartDict[name]
            
            updateObj["/itemName"] = NSNull()
            updateObj["/itemUrl"] = NSNull()
            updateObj["/itemType"] = NSNull()
            updateObj["/itemUnit"] = NSNull()
            
            
            
            var index = 0
            for item in cart {
                if item.name! == storedItem?.name && item.type! == storedItem?.type {
                    cart.remove(at: index)
                    index = index + 1
                }
            }
            
            cartDict.removeValue(forKey: name)
            ref.updateChildValues(updateObj)
        }
        

    }
}
