//
//  User.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 21/9/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//


import Foundation
import Firebase
import FirebaseDatabase

class User: NSObject {
    
    //    private var _userName: String?
    //    private var userKey: String?
    //
    //    var course: String?
    //    var email: String?
    //    var gender: String?
    //    var name: String?
    //    var provider: String?
    //    var university: String?
    //    var year: String?
    
    var userKey: String?
    private var _company: String?
    private var _phoneNumber: String?
    private var _name: String?


    private var _notiCount: Int?
    private var _msgCount: [String: Bool]!
    

    
    private var _userRef: DatabaseReference!
    
    var name: String {
        return _name!
    }
    
    var phoneNumber: String {
        
        return _phoneNumber!

    }
    
    var company: String {
        return _company!
    }
   
    
    
    
    var userRef: DatabaseReference {
        return _userRef
    }
    
  
    var notiCount: Int {
        get {
            return _notiCount!
        }
        
        set(newCount) {
            _notiCount = newCount
        }
        
    }
    
    var msgCount: [String: Bool] {
        return _msgCount
    }
    
    
    override init() {
        
    }
    
    
    init(userKey: String, userData: Dictionary<String, AnyObject>) {
        
        
        self.userKey = userKey
        
        if let company = userData["company"] as? String {
            self._company = company
        }
        
        if let name = userData["name"] as? String {
            self._name = name
        }
        
        if let phone = userData["phoneNumber"] as? String {
            self._phoneNumber = phone
        }
       
        if let notiCount = userData["notiCount"] as? Int {
            self._notiCount = notiCount
        }
        
        if let msgCount = userData["msgCount"] as? [String: Bool] {
            self._msgCount = msgCount
        }
        
        
        
        
        //  _userRef = DataService.ds.REF_USERS.child(_userKey!)
    }
    
    func adjustMsgCount(addCount: Bool,fromId: String) {
        
        let ref2 = DataService.ds.REF_USERS.child(self.userKey!)
        
        ref2.runTransactionBlock({ (currentData) -> TransactionResult in
            
            if var userDict = currentData.value as? Dictionary<String, AnyObject> {
                
                var msgCount = userDict["msgCount"] as? [String: Bool]
                
                if addCount {
                    msgCount?[fromId] = true
                    //                    self.addUser(uid: uid)
                    
                } else {
                    // userLike.removeValue(forKey: uid)
                    msgCount?.removeValue(forKey: fromId)
                    //                    self.removeUser(uid: uid)
                }
                
                userDict["msgCount"] = msgCount as AnyObject
                
                
                
                currentData.value = userDict
                print("scueess")
                return TransactionResult.success(withValue: currentData)
            }
            
            return TransactionResult.success(withValue: currentData)
        }) { (error, commited, snap) in
            
        }
    }

}

