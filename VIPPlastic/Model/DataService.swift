//
//  DataService.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 20/8/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import Foundation
import Firebase

let DB_BASE = Database.database().reference()
let STORAGE_BASE = Storage.storage().reference()

class DataService {
    
    static let ds = DataService()
    var login: Bool!
    var currentUser: User!
    var admin: User!
    var OS: String!
    var PUSH_TOKEN = ""
    
    var REF_USERS = DB_BASE.child("users")
    
    var REF_Item = DB_BASE.child("item")
    var REF_CategoryPreview = DB_BASE.child("categoryPreview")
    var REF_Category = DB_BASE.child("category")
    var REF_USER_MESSAGES = DB_BASE.child("user-messages")
    var REF_MESSAGES = DB_BASE.child("messages")
    var REF_MESSAGE_PIC = STORAGE_BASE.child("messagePic")
    
    var REF_CART_USER = DB_BASE.child("cart")
    var REF_FEEDBACK = DB_BASE.child("feedback")
    
    var REF_Item_Pic = STORAGE_BASE.child("itemPic")

    init() {
        var updateObj = [String:AnyObject]()
        
        updateObj["/phoneNumber"] = "+60197726862" as AnyObject
        updateObj["/name"] = "Xavier Tan" as AnyObject
        updateObj["/company"] = "VIP" as AnyObject
        
        admin = User(userKey: ADMIN_ID, userData: updateObj)
    }
    func createFirebaseUser(uid: String, phoneNumber: String, company: String, name: String,  completion: @escaping () -> ()) {
        
        var updateObj = [String:AnyObject]()
        let ref = DataService.ds.REF_USERS.child(uid)
        
        updateObj["/phoneNumber"] = phoneNumber as AnyObject
        updateObj["/name"] = name as AnyObject
        updateObj["/company"] = company as AnyObject
        updateObj["/msgCount/key"] = true as AnyObject
//        updateObj["/notiCount"] = 0 as AnyObject
//        updateObj["/pushToken"] = "" as AnyObject

       
        
        ref.updateChildValues(updateObj)
        

        self.signInWithData(uid: uid, userData: updateObj)
        completion()
        
    }
    
    func signIn(uid: String)  {
        DataService.ds.REF_USERS.child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            if ( snapshot.value is NSNull ) {
            } else {
                print("YEAH")
                if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                    let key = snapshot.key
                    //                    var currentUser = User(userKey: key, userData: userDict)
                    self.login = true
                    let defaults = UserDefaults.standard
                    defaults.set(uid, forKey: "uid")
                    self.currentUser = User(userKey: uid, userData: userDict)
                }
            }
        })
        

    }
    
    func signInWithData(uid: String, userData: Dictionary<String, AnyObject>)  {
        self.login = true
        let defaults = UserDefaults.standard
        defaults.set(uid, forKey: "uid")
        self.currentUser = User(userKey: uid, userData: userData)
    }
    
    func signOut() {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            
            let defaults = UserDefaults.standard
            for key in defaults.dictionaryRepresentation().keys{
                defaults.removeObject(forKey: key.description)
            }
            
            self.currentUser = nil
            DataService.ds.login = false
            
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    func addInstanceID() {
        let ref = DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!)
        var updateObj = ["/pushToken":"\(self.PUSH_TOKEN)"]
        ref.updateChildValues(updateObj)
    }
    
}
