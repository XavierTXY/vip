//
//  RoundView.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 21/8/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import Foundation
import UIKit
class RoundView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()

        layer.cornerRadius = 12.0
        layer.masksToBounds = false
//        layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
//        layer.shadowOffset = CGSize(width:0, height: 0)
//        layer.shadowOpacity = 0.4

    }
}
