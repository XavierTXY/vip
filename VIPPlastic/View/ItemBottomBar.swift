//
//  ItemBottomBar.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 20/8/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//


import Foundation
import UIKit

class ItemBottomBar: UIView {
    
    var itemVC: ItemVC!

    @IBAction func addToCartTapped(_ sender: Any) {
        itemVC.bottomCartTapped()
    }
    @IBAction func getQuoteTapped(_ sender: Any) {
        itemVC.goToNumberVC()
//        let urlString = "Sending WhatsApp message through app in Swift"
//        let urlStringEncoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
////        let urlStringEncoded = urlString.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())
//        let url  = NSURL(string: "whatsapp://send?text=\(urlStringEncoded!)&phone_number=+6591034123")
//
//        if UIApplication.shared.canOpenURL(url! as URL) {
//            UIApplication.shared.openURL(url! as URL)
//        } else {
//            let errorAlert = UIAlertView(title: "Cannot Send Message", message: "Your device is not able to send WhatsApp messages.", delegate: self, cancelButtonTitle: "OK")
//            errorAlert.show()
//        }
    }
}
