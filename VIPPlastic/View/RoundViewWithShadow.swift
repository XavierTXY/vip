//
//  RoundViewWithShadow.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 1/10/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import Foundation
import UIKit
class RoundViewWithShadow: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.cornerRadius = 10.0
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
        layer.shadowOffset = CGSize(width:0, height: 0)
        layer.shadowOpacity = 0.4
        
    }
}
