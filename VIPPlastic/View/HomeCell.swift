//
//  HomeCell.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 25/7/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import UIKit

class HomeCell: UITableViewCell {

    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var img4: UIImageView!
    @IBOutlet weak var lbl4: UILabel!
  
    @IBOutlet weak var catLbl: UILabel!
    
    var images = [UIImageView]()
    var lbls = [UILabel]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        images = [img1,img2,img3,img4]
        lbls = [lbl1,lbl2,lbl3,lbl4]
//        
//        img1.layer.cornerRadius = 5.0
//        img1.layer.masksToBounds = false
//        img2.layer.cornerRadius = 5.0
//        img2.layer.masksToBounds = false
//        img3.layer.cornerRadius = 5.0
//        img3.layer.masksToBounds = false
//        img4.layer.cornerRadius = 5.0
//        img4.layer.masksToBounds = false
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(items: [Item], cat: String) {
        
    
        
        catLbl.text = cat
        if items.count != 0 {
            for indx in 0...items.count - 1 {

//                images[indx].image = UIImage(named: "vipLogo")
                if indx < 4 {
                    lbls[indx].text = items[indx].brand!
                    images[indx].loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: items[indx].urls["url0"]!)
                }
                
            }
        }

    }

}
