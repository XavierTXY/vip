//
//  MessageCell.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 21/9/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//


import UIKit
import Firebase

class MessageCell: UITableViewCell {
    @IBOutlet weak var itemNameLbl: UILabel!
    
    @IBOutlet weak var notiImg: UIView!
    
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    
    
    
    var msg: MessageModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        notiImg.layer.cornerRadius = notiImg.frame.height / 2
        notiImg.clipsToBounds = true
    }
    
    
    func configureCell(message: MessageModel, toName: String, company: String) {
        
        self.msg = message
        itemNameLbl.text = "\(company) - \(toName)"
        
        if message.fromId == (Auth.auth().currentUser?.uid)! {
            notiImg.isHidden = true
        } else {
            if message.toSeen! {
                notiImg.isHidden = true
            } else {
                notiImg.isHidden = false
            }
        }

        
        
        
        if message.text == nil {
            messageLbl.text = "Sent an image"
        } else {
            messageLbl.text = message.text
        }
        
        
        
        
        
        if let seconds = message.timeStamp?.doubleValue {
            let timesStampDate = NSDate(timeIntervalSince1970: seconds)
            var dateFormatter = DateFormatter()
            
            
            let calendar = Calendar.current
            
            if calendar.isDateInToday(timesStampDate as Date) {
                dateFormatter.dateFormat = "hh:mm"
            } else {
                dateFormatter.dateFormat = "d MMM"
            }
            
            timeLbl.text = dateFormatter.string(from: timesStampDate as Date)
        }
        
        
    }
    
    
    
}
