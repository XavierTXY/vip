//
//  NumberVC
//  VIPPlastic
//
//  Created by XavierTanXY on 21/8/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import UIKit
import Firebase
import CountryList
import SVProgressHUD


class NumberVC: UIViewController, UITabBarControllerDelegate, CountryListDelegate, UITextFieldDelegate {

 
    var rootTabBar: UITabBarController!
    var countryList = CountryList()
    var countryCode: String!
    
    @IBOutlet weak var countryCodeBtn: UIButton!
    @IBOutlet weak var numberTextField: UITextField!
  
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelTapped))
        
        countryCode = "+60"
        countryList.delegate = self
        numberTextField.delegate = self 
        // Do any additional setup after loading the view.
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if DataService.ds.login {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.numberTextField.becomeFirstResponder()
        }
    }
    
    func selectedCountry(country: Country) {
        self.countryCodeBtn.setTitle("+\(country.phoneExtension)", for: .normal)
        self.countryCode = "+\(country.phoneExtension)"
    }
    
    @objc func cancelTapped() {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    var selectedID: String!
    var phoneNumber: String!
    @IBAction func loginTapped(_ sender: Any) {
//        if let email = emailTextField.text, let pwd = pwdTextField.text , (email.characters.count > 0 && pwd.characters.count > 0) {
//
//            Auth.auth().signIn(withEmail: email, password: pwd) { (user, error) in
//            }
//        }
        SVProgressHUD.show()
//        print("\(self.countryCode!)\(numberTextField.text!)")
        
        if numberTextField.text == "" {
            ErrorAlert().createAlert(title: "Reminder", msg: "Please enter your number correctly", object: self)
        } else {
            PhoneAuthProvider.provider().verifyPhoneNumber("\(self.countryCode!)\(numberTextField.text!)", uiDelegate: nil) { (verificationID, error) in
                if let error = error {
                    //                self.showMessagePrompt(error.localizedDescription)
                    SVProgressHUD.dismiss()
                    ErrorAlert().createAlert(title: "Error", msg: error.localizedDescription, object: self)
                    print(error.localizedDescription)
                    return
                }
                // Sign in using the verificationID and the code sent to the user
                // ...
                print("id \(verificationID!)")
                self.selectedID = verificationID!
                self.phoneNumber = self.numberTextField.text!
                SVProgressHUD.dismiss()
                self.performSegue(withIdentifier: "VerificationVC", sender: nil)
            }
        }


    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? VerificationVC {
            
            destinationVC.veriID = self.selectedID
            destinationVC.phoneNumber = self.phoneNumber
            
        }
    }
    

    @IBAction func countryCodeTapped(_ sender: Any) {
//        let navController = UINavigationController(rootViewController: countryList)
        self.present(countryList, animated: true, completion: nil)
    }
    
}
