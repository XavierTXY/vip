//
//  SignUpVC.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 14/9/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import UIKit
import Firebase

class SignUpVC: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var companyTextField: UITextField!
    
    var phoneNumber: String!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    @IBAction func signUPTapped(_ sender: Any) {
        
        var uid = (Auth.auth().currentUser?.uid)!
        DataService.ds.createFirebaseUser(uid: uid, phoneNumber: phoneNumber, company: companyTextField.text!, name: nameTextField.text!) {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
}
