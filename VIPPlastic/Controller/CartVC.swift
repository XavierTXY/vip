//
//  CartVC.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 28/9/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import UIKit
import Firebase
import PopupDialog

class CartCell: UITableViewCell {
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var unitLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(itemName: String, type: String, unit: Int, url: String) {
        itemImage.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: url)
        itemNameLbl.text = itemName
        typeLbl.text = type
        unitLbl.text = "\(unit) units"
    }
    
}
class CartVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var items = [Item]()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if Cart.shoppingCart.cart.count != 0 {
            items = Cart.shoppingCart.cart
        } else {
            fetchShoppingCart()
        }
        self.tableView.reloadData()
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func fetchShoppingCart() {
        DataService.ds.REF_CART_USER.child(DataService.ds.currentUser.userKey!).observeSingleEvent(of: .value, with: {(snapshot) in
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                
                for snap in snapshots {
                    if let itemDict = snap.value as? Dictionary<String, AnyObject> {
                        
                        
                        let key = snap.key
                        let item = Item(name: itemDict["itemName"] as! String, url: itemDict["itemUrl"] as! String, unit: itemDict["itemUnit"] as! Int, type: itemDict["itemType"] as! String)
                        
                        self.items.append(item)
                        
                    }
                    self.attemptToReload()
                }
                
                
                
            }
            
            
        })
    }
    
    func attemptToReload() {

        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell") as! CartCell
        
        cell.selectionStyle = .none
        let item = items[indexPath.row]
        cell.configureCell(itemName: item.name!, type: item.type!, unit: item.unit!, url: item.url!)

        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            tableView.beginUpdates()
            let popup = PopupDialog(title: "Are you sure?", message: "Do you want to delete this item from cart?", image: nil)
            
            
            let buttonOne = CancelButton(title: "Yes") {
                var itemToBeRemoved = self.items[indexPath.row]
                self.items.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.left)
                Cart.shoppingCart.updateCart(name: itemToBeRemoved.name!, url: itemToBeRemoved.url!, unit: itemToBeRemoved.unit, type: itemToBeRemoved.type!, add: false)
            }
            let buttonTwo = CancelButton(title: "No") {
                
            }
            
            popup.addButtons([buttonOne,buttonTwo])
            
            // Present dialog
            self.present(popup, animated: true, completion: nil)
            
            tableView.endUpdates()
        }
        
    }
    @IBAction func getQuoteTapped(_ sender: Any) {
        if DataService.ds.currentUser.userKey != ADMIN_ID {
            if(self.items.count != 0) {
            
                for item in items {
                    handleSend(values: ["text":item.name!], item: item)
                    
                }
                
            
            } else {

                ErrorAlert().createAlert(title: "Reminder", msg: "Your cart is empty!", object: self)
            }
        } else {
            ErrorAlert().createAlert(title: "Reminder", msg: "YOU ARE ADMIN", object: self)
        }
    }
    
    var itemCount = 0
    private func handleSend(values: [String: Any], item: Item)  {
//        wentChatLog = true
        
        let ref = DataService.ds.REF_MESSAGES
    
        let firebaseMessage = DataService.ds.REF_MESSAGES.childByAutoId()
        let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
        
        let toId = ADMIN_ID
        let fromId = Auth.auth().currentUser?.uid
        
        DataService.ds.admin.adjustMsgCount(addCount: true, fromId: fromId!)
        
        
        
        var dict: [String: Any] = ["toId": toId, "fromId": fromId, "timeStamp": timestamp, "fromUserName": DataService.ds.currentUser.name, "fromSeen": true, "toSeen": false, "itemType": item.type!, "itemUnit": "\(item.unit!)", "isItem": true,"imageUrl": item.url! ] as [String : Any]
        
        
        
        
        for (key, element) in values {
            
            dict[key] = element
        }

        
        firebaseMessage.updateChildValues(dict, withCompletionBlock: {(error, ref) in
            
            if error != nil {
                print(error)
            }
            
            
            let userMsgRef = DataService.ds.REF_USER_MESSAGES.child(fromId!).child(toId)
            let msgId = firebaseMessage.key
            userMsgRef.updateChildValues([msgId: true])
            
            let recipientUserMsgRef = DataService.ds.REF_USER_MESSAGES.child(toId).child(fromId!)
            recipientUserMsgRef.updateChildValues([msgId: true])
            
            Cart.shoppingCart.updateCart(name: item.name!, url: item.url!, unit: item.unit, type: item.type!, add: false)
            
            self.itemCount = self.itemCount + 1
            if self.itemCount == self.items.count {
                
                self.performSegue(withIdentifier: "ChatLogVC", sender: nil)
                Cart.shoppingCart.removeAll()
            }
            
            
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? ChatLogVC {
            
            destinationVC.user = DataService.ds.admin
//            destinationVC.item = self.item
//            destinationVC.itemType = self.selectedType
//            destinationVC.selectedUnits = Int(self.stepperView.value)
            
        }
    }

    
}
