//
//  ListItemVC.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 20/8/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import UIKit
import TagListView
import Firebase

class ListCell: UITableViewCell {
    @IBOutlet weak var itemPic: UIImageView!
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var listView: TagListView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
        itemPic.layer.cornerRadius = 10.0
        itemPic.layer.masksToBounds = false
        
        
        listView.textFont = UIFont.systemFont(ofSize: 12)
        listView.alignment = .left

    
    }
    
    func configureCell(item: Item) {
        itemPic.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: item.urls["url0"]!)
        itemNameLbl.text = item.name!
        priceLbl.text = item.price!
        listView.addTags([item.condition!,item.brand!])
    }

}

class ListItemVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var category: String!
    var selectedItem: Item!
    var items = [Item]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = category
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        
        fetchData()
    }

    func fetchData() {
        DataService.ds.REF_Category.child(category).observeSingleEvent(of: .value, with: {(snapshot) in
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                
                for snap in snapshots {
                    if let itemDict = snap.value as? Dictionary<String, AnyObject> {
                        
                        
                        let key = snap.key
                        let item = Item(itemKey: key, itemData: itemDict)
                            self.items.append(item)
                        
                        
                        
                    }
                    self.tableView.reloadData()
                }
                
                
                
            }
            
            
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell") as! ListCell
        cell.configureCell(item: items[indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.selectedItem = items[indexPath.row]
        performSegue(withIdentifier: "ItemVC", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? ItemVC {
            
            destinationVC.item = self.selectedItem
            
        }
    }



}
