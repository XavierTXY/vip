//
//  PrivacyVC.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 4/10/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import UIKit

class PrivacyVC: UIViewController {
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Privacy Policy"
        // Do any additional setup after loading the view.
    }
    

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.textView.setContentOffset(CGPoint.zero, animated: false)
    }

}
