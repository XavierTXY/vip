//
//  ViewController.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 25/7/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import UIKit
import Firebase

class HomeVC: UIViewController, UITableViewDataSource, UITableViewDelegate,UITabBarControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    
    var items = [Item]()
    var itemDict = [String: Item]()
    var crusherArray = [Item]()
    var mixserArray = [Item]()
    var hopperArray = [Item]()
    var autoloaderArray = [Item]()
    var chillerArray = [Item]()
    var mtcArray = [Item]()
    
    var categories = [CRUSHER, MIXER, HOPPER, AUTOLOADER, CHILLER, MTC]
    
    var selectedCat: String!
    
    
    var user: User!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.tabBarController?.delegate = self
        self.navigationItem.title = "VIP Plastic Machinery"

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 185
        self.tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        
        
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.black
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        
//        if DataService.ds.login {
//            if DataService.ds.currentUser.userKey == ADMIN_ID {
                self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addTapped))
//            }
//        }

        
        fetchItems()
        
        if DataService.ds.login {
            DataService.ds.REF_USERS.child((Auth.auth().currentUser?.uid)!).observe(.value, with: { (snapshot) in
                if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                    print("detect user changes")
                    
                    if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                        let key = snapshot.key
                        self.user = User(userKey: key, userData: userDict)
                        
                        
                        
                        
                        if ((self.user?.msgCount.count)! - 1) != 0 {
                            self.setMsgBadgeNumber(user:self.user)
                        }
                        
                        
                        
                    }
                }
                
            })
        }

    }
    
    @objc func refresh() {
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(fetchItems), userInfo: nil, repeats: false)
//        fetchItems()
    }
    
    func attemptToReload() {
        if (self.refreshControl?.isRefreshing)! {
            self.refreshControl.endRefreshing()
        }
        self.tableView.reloadData()
    }
    
    func setMsgBadgeNumber(user: User) {
        if let tabItems = self.tabBarController?.tabBar.items as NSArray!
        {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[1] as! UITabBarItem
            tabItem.badgeValue = "\(user.msgCount.count - 1)"
            
//            print(DataService.ds.currentUser.msgCount.count - 1)
            
        }
    }
    
    @objc func fetchItems() {
        crusherArray = []
        mixserArray = []
        hopperArray = []
        autoloaderArray = []
        chillerArray = []
        mtcArray = []
        
        for cat in categories {
            DataService.ds.REF_Category.child(cat).observeSingleEvent(of: .value, with: {(snapshot) in
                if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                    
                    for snap in snapshots {
                        if let itemDict = snap.value as? Dictionary<String, AnyObject> {
                            
                            
                            let key = snap.key
                            let item = Item(itemKey: key, itemData: itemDict)
                            
                            if item.category == CRUSHER {
                                print("Added Crusher")
                                self.crusherArray.append(item)
                            } else if item.category == MIXER {
                                print("Added Mixser")
                                self.mixserArray.append(item)
                            } else if item.category == HOPPER {
                                self.hopperArray.append(item)
                            } else if item.category == AUTOLOADER {
                                self.autoloaderArray.append(item)
                            } else if item.category == CHILLER {
                                self.chillerArray.append(item)
                            }
                            if item.category == MTC {
                                self.mtcArray.append(item)
                                print("count \(self.mtcArray.count)")
                            }
                            
                            
                        }
                    self.attemptToReload()
                    }
                    
                    
                   
                }
                
                
            })
        }

        
    }
    @objc func addTapped() {
        print("add tapped")
        performSegue(withIdentifier: "AddVC", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell") as! HomeCell
        cell.img1.image = UIImage(named: "placeholder")
        cell.lbl1.text = "-"
        cell.img2.image = UIImage(named: "placeholder")
        cell.lbl2.text = "-"
        cell.img3.image = UIImage(named: "placeholder")
        cell.lbl3.text = "-"
        cell.img4.image = UIImage(named: "placeholder")
        cell.lbl4.text = "-"
        
        if categories[indexPath.row] == CRUSHER {
            cell.setCell(items: crusherArray, cat: CRUSHER)
        } else if categories[indexPath.row] == MIXER {
            print("why")
            cell.setCell(items: mixserArray, cat: MIXER)
        } else if categories[indexPath.row] == HOPPER {
            print("why")
            cell.setCell(items: hopperArray, cat: HOPPER)
        } else if categories[indexPath.row] == AUTOLOADER {
            print("why")
            cell.setCell(items: autoloaderArray, cat: AUTOLOADER)
        } else if categories[indexPath.row] == CHILLER {
            print("why")
            cell.setCell(items: chillerArray, cat: CHILLER)
        } else if categories[indexPath.row] == MTC {
            print("why")
            cell.setCell(items: mtcArray, cat: MTC)
        }
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selectedCat = categories[indexPath.row]
        performSegue(withIdentifier: "ListItemVC", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? ListItemVC {
            
            destinationVC.category = self.selectedCat

        }
        
        if let destinationVC = segue.destination as? AddVC {
            destinationVC.item = nil
        }
    }

    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if viewController is AccountVC {
            if !DataService.ds.login {
                if let vc = viewController as? AccountVC {
                    vc.been = false
                }
            }
        }
        
        if viewController is MessagesVC {
            if !DataService.ds.login {
                if let vc = viewController as? MessagesVC {
                    vc.been = false
                }
            }
        }
    }


    
}

