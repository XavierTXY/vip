//
//  NumberVC
//  VIPPlastic
//
//  Created by XavierTanXY on 21/8/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import UIKit
import Firebase


class NumberVC: UIViewController {

    @IBOutlet weak var pwdTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginTapped(_ sender: Any) {
//        if let email = emailTextField.text, let pwd = pwdTextField.text , (email.characters.count > 0 && pwd.characters.count > 0) {
//
//            Auth.auth().signIn(withEmail: email, password: pwd) { (user, error) in
//            }
//        }
        
        PhoneAuthProvider.provider().verifyPhoneNumber(emailTextField.text!, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
//                self.showMessagePrompt(error.localizedDescription)
                print(error.localizedDescription)
                return
            }
            // Sign in using the verificationID and the code sent to the user
            // ...
            print("id \(verificationID!)")
//            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
        }
    }
    


}
