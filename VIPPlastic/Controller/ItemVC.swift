//
//  ItemVC.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 20/8/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import UIKit
import ImageSlideshow
import SDWebImage
import Spring

class ItemVC: UIViewController,UITextViewDelegate {
    
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var detailTV: UITextView!
    @IBOutlet weak var conditionLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var layerView: UIView!
    
    @IBOutlet weak var slideshow: ImageSlideshow!
    var itemBottomBar: ItemBottomBar!
    var item: Item!
    var urls = [SDWebImageSource]()
    
    @IBOutlet weak var detailTextField: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = item.name!
        
        for idx in 0...item.urls.count-1 {
            urls.append(SDWebImageSource(urlString: item.urls["url\(idx)"]!)!)
          
        }
        detailTextField.delegate = self
        
        
        
        let localSource = [ImageSource(imageString: "Shimano-logo-vector")!, ImageSource(imageString: "panasonic")!, ImageSource(imageString: "Mitsubishi_Electric_logo.svg")!, ImageSource(imageString: "Meiban_Group_BW")!]
        
//        slideshow.slideshowInterval = 5.0
        slideshow.pageControlPosition = .underScrollView
        slideshow.contentScaleMode = UIViewContentMode.scaleAspectFit
        
//        let pageControl = UIPageControl()
//        pageControl.currentPageIndicatorTintColor = UIColor.lightGray
//        pageControl.pageIndicatorTintColor = UIColor.black
        slideshow.pageControl.currentPageIndicatorTintColor = MAIN_COLOR
        slideshow.pageControl.pageIndicatorTintColor = UIColor.white
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        slideshow.activityIndicator = DefaultActivityIndicator()
        slideshow.currentPageChanged = { page in
            print("current page:", page)
        }
        
        // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`

    
        slideshow.setImageInputs(urls)
//        SDWebImageSource(urlString: "https://images.unsplash.com/photo-1432679963831-2dab49187847?w=1080")
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
        slideshow.addGestureRecognizer(recognizer)
        
        configureItem()
        
//        UIBarButtonItem(image: UIImage(named: "shopping-cart"), style: .plain, target: self, action: #selector(cartTapped)
        
        
//        if DataService.ds.currentUser.userKey! == ADMIN_ID {
//            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(editTapped))
//        } else {
//            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "shopping-cart"), style: .plain, target: self, action: #selector(cartTapped))
//        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addAccessoryView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.detailTV.setContentOffset(CGPoint.zero, animated: false)
    }
    
    @objc func editTapped() {
        performSegue(withIdentifier: "AddVC", sender: nil)
    }
    @objc func cartTapped() {
        if !DataService.ds.login {
            performSegue(withIdentifier: "NumberVC", sender: nil)
        } else {
            performSegue(withIdentifier: "CartVC", sender: nil)
        }
    }
    
    func bottomCartTapped() {
        
        if !DataService.ds.login {
            performSegue(withIdentifier: "NumberVC", sender: nil)
        } else {
            self.performSegue(withIdentifier: "TypeVC", sender: nil)
        }
    }
    
    func configureItem() {
        self.conditionLbl.text = "\(item.condition!) - \(item.brand!)"
        self.priceLbl.text = item.price!
        self.nameLbl.text = item.name!
        self.detailTV.text = item.description!
//        self.detailTV.setContentOffset(CGPoint.zero, animated: false)
        
    
    }
    
    @objc func didTap() {
        let fullScreenController = slideshow.presentFullScreenController(from: self)
        // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    
    public func addAccessoryView() {
        
        itemBottomBar = Bundle.main.loadNibNamed("ItemBottomBar", owner: self, options: nil)?.first as! ItemBottomBar
        itemBottomBar.itemVC = self
        detailTextField.inputAccessoryView = itemBottomBar
        detailTextField.becomeFirstResponder()
        
    }
    
    @IBAction func typeTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "TypeVC", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? TypeVC {
            
            destinationVC.item = self.item
            
        }
        
        if let destinationVC = segue.destination as? AddVC {
            destinationVC.item = self.item
        }
    }


    @objc func maximizeView(_ sender: AnyObject) {
        SpringAnimation.spring(duration: 0.7, animations: {
//            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
//            self.layerView.backgroundColor = UIColor.clear
        })
        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.default, animated: true)
    }
    
    @objc func minimizeView(_ sender: AnyObject) {
        SpringAnimation.spring(duration: 0.7, animations: {
//            self.view.transform = CGAffineTransform(scaleX: 0.935, y: 0.935)
//            self.layerView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.15)
        })
        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.lightContent, animated: true)
    }
    
    func goToNumberVC(){
        if !DataService.ds.login {
            performSegue(withIdentifier: "NumberVC", sender: nil)
        } else {
            performSegue(withIdentifier: "TypeVC", sender: nil)
        }
        
    }

}
