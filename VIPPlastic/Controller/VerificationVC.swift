//
//  VerificationVC.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 14/9/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class VerificationVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var veriCodeTextField: UITextField!
    var veriID: String!
    var phoneNumber: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.setMaximumDismissTimeInterval(1.0)

        // Do any additional setup after loading the view.
        veriCodeTextField.delegate = self
        veriCodeTextField.becomeFirstResponder()
    }


    @IBAction func verifyTapped(_ sender: Any) {
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: self.veriID, verificationCode: veriCodeTextField.text!)
        
        SVProgressHUD.show()
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error {
                // ...
                SVProgressHUD.dismiss()
                ErrorAlert().createAlert(title: "Error", msg: error.localizedDescription, object: self)
                return
            }
            // User is signed in
            // ...

           
            self.checkLoggedIn()
//            self.performSegue(withIdentifier: "SignUpVC", sender: nil)

        }
    }
    
    func checkLoggedIn() {
        var uid = (Auth.auth().currentUser?.uid)!
        DataService.ds.REF_USERS.child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            if ( snapshot.value is NSNull ) {
                self.performSegue(withIdentifier: "SignUpVC", sender: nil)
            } else {
                print("YEAH")
                if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                    let key = snapshot.key
//                    var currentUser = User(userKey: key, userData: userDict)
                    DataService.ds.signInWithData(uid: uid, userData: userDict)
                     SVProgressHUD.dismiss()
                    self.navigationController?.popToRootViewController(animated: true)
//                    DataService.ds.currentUser = currentUser
//                    defaults.set(currentUser.universityShort, forKey: "uniShort")
                    
                    
                }

            }
           
        })
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? SignUpVC {
            
//            destinationVC.veriID = self.selectedID
            destinationVC.phoneNumber = self.phoneNumber
            
        }
    }
    
}
