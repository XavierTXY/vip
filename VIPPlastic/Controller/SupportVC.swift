//
//  SupportVC.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 4/10/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import UIKit
import SVProgressHUD

class SupportVC: UIViewController,UITextViewDelegate {
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.delegate = self
        textView.becomeFirstResponder()
        self.navigationItem.title = "Feedback"
        SVProgressHUD.setMaximumDismissTimeInterval(1.0)
       self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Send", style: .plain, target: self, action: #selector(sendTapped))
    }
    
    @objc func sendTapped() {
        
        if textView.text == "" {
            ErrorAlert().show(title: "Reminder", msg: "Please enter your feedback before sending.", object: self)
        } else {
            let ref = DataService.ds.REF_FEEDBACK.child(DataService.ds.currentUser.userKey!).childByAutoId()
            ref.updateChildValues(["feedback":textView.text])
            SVProgressHUD.showSuccess(withStatus: "We will get back to you soon!")
            self.navigationController?.popViewController(animated: true)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
