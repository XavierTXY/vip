//
//  AccountVC.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 20/9/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import PopupDialog

class AccountCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var imageLbl: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(name: String) {
        nameLbl.text = name
//        imageLbl.image = img
    }
}

class AccountVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var versionLbl: UILabel!
    var been: Bool!
    var settings = [[String]]()
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Settings"
        
        SVProgressHUD.setMaximumDismissTimeInterval(1.0)
        tableView.delegate = self
        tableView.dataSource = self
//        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
        
        been = false
        
        settings = [["Account", "Term and Condition", "Privacy Policy", "Support"], ["Log Out"]]
        // Do any additional setup after loading the view.
        versionLbl.text = "Version: \(DataService.ds.OS!)"
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if !been {
            if DataService.ds.login == false {
                print("wulal")
                been = true
                self.performSegue(withIdentifier: "NumberVC", sender: nil)
            } 
        }
        
        if !DataService.ds.login {
            
            self.performSegue(withIdentifier: "NumberVC", sender: nil)
            tabBarController?.selectedIndex = 0
        }
        


    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return settings.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settings[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountCell") as! AccountCell

        cell.configureCell(name: settings[indexPath.section][indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == 0 {
            ErrorAlert().createAlert(title: "Account", msg: "Name: \(DataService.ds.currentUser.name) \nCompany: \(DataService.ds.currentUser.company) \nPhone Number: \(DataService.ds.currentUser.phoneNumber)", object: self)
        } else if indexPath.section == 0 && indexPath.row == 1 {
            self.performSegue(withIdentifier: "TermVC", sender: nil)
        } else if indexPath.section == 0 && indexPath.row == 2 {
            self.performSegue(withIdentifier: "PrivacyVC", sender: nil)
        } else if indexPath.section == 0 && indexPath.row == 3 {
            self.performSegue(withIdentifier: "SupportVC", sender: nil)
        }
        if indexPath.section == 1 {
            
            let popup = PopupDialog(title: "Are you sure?", message: "Do you want to logged out?")
            
            
            let btn1 = PopupDialogButton(title: "Yes") {
                DataService.ds.signOut()
                SVProgressHUD.showSuccess(withStatus: "Logged Out!")
                self.tabBarController?.selectedIndex = 0
            }
            
            let btn2 = PopupDialogButton(title: "No") {
                
            }
            
            let buttons = [btn2,btn1]
            popup.addButtons(buttons)
            self.present(popup, animated: true, completion: nil)

        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? NumberVC {
            
            destinationVC.rootTabBar = self.tabBarController
            
        }
    }
    

    


}
