//
//  TypeVC.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 11/9/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import UIKit
import Spring
import TagListView
import GMStepper
import Firebase
import SVProgressHUD

class TypeVC: UIViewController,TagListViewDelegate {

    @IBOutlet weak var modalView: SpringView!
    @IBOutlet weak var typesView: TagListView!
    @IBOutlet weak var stepperView: GMStepper!
    
    var item: Item!
    var selectedType: String!
    var wentChatLog: Bool!
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.setMaximumDismissTimeInterval(1.0)
        wentChatLog = false
       
        
        selectedType = "NONE"
        typesView.delegate = self
        typesView.textFont = UIFont.systemFont(ofSize: 18)
        typesView.alignment = .left
//        var array = self.item.types.keys
//        for (type,_) in item.types {
//            print(type)
//            typesView.addTag(type)
//        }
        
        for type in self.item.typeArray {
            typesView.addTag(type)
        }
//
//        for type in array{
//            print(type)
//            typesView.addTag(type)
//        }
        modalView.transform = CGAffineTransform(translationX: 0, y: 300)
//        self.definesPresentationContext = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        UIApplication.shared.sendAction(#selector(ItemVC.minimizeView(_:)), to: nil, from: self, for: nil)
        
        modalView.animate()

    }
    

    override func viewDidLayoutSubviews() {
        modalView.roundCorners([.topLeft, .topRight], radius: 10)
    }
    
    func setSelected(tagView: TagView){
     

            self.typesView.borderColor = UIColor.black
            self.typesView.textColor = UIColor.black
            
            tagView.borderColor = MAIN_COLOR
            tagView.textColor = MAIN_COLOR
      
    }
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
//        tagView.tagBackgroundColor = UIColor.white
        tagView.onTap = { tagView in
            self.setSelected(tagView: tagView)
            print("Don’t tap me!")
            self.selectedType = title
        }
    }
    @IBAction func dismissTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
     UIApplication.shared.sendAction(#selector(ItemVC.maximizeView(_:)), to: nil, from: self, for: nil)
    }
    

    @IBAction func addToCartTapped(_ sender: Any) {
        if !DataService.ds.login {
            performSegue(withIdentifier: "NumberVC", sender: nil)
        } else {
            
             if DataService.ds.currentUser.userKey != ADMIN_ID {
                if(self.selectedType != "NONE") {
                    Cart.shoppingCart.updateCart(name: item.name!, url: item.urls["url0"]!, unit: Int(stepperView.value), type: selectedType, add: true)
                    SVProgressHUD.showSuccess(withStatus: "Added To Cart!")
                } else {
                    ErrorAlert().createAlert(title: "Reminder", msg: "Please choose one of the types", object: self)
                }
             } else {
                ErrorAlert().createAlert(title: "Reminder", msg: "YOU ARE ADMIN", object: self)
            }


        }
    }
    @IBAction func getQuiteTapped(_ sender: Any) {

        if !DataService.ds.login {
            performSegue(withIdentifier: "NumberVC", sender: nil)
        } else {
            if DataService.ds.currentUser.userKey != ADMIN_ID {
                if(self.selectedType != "NONE") {
                    handleSend(values: ["text":self.item.name!])
                    
                    self.performSegue(withIdentifier: "ChatLogVC", sender: nil)
                } else {
                    
                    ErrorAlert().createAlert(title: "Reminder", msg: "Please choose one of the types", object: self)
                }
            } else {
                ErrorAlert().createAlert(title: "Reminder", msg: "YOU ARE ADMIN", object: self)
            }
        }
        


        
    }
    
    
    
    private func handleSend(values: [String: Any])  {
        wentChatLog = true
        
        let ref = DataService.ds.REF_MESSAGES
        let firebaseMessage = DataService.ds.REF_MESSAGES.childByAutoId()
        let timestamp = NSNumber(integerLiteral: Int(NSDate().timeIntervalSince1970))
        
        let toId = ADMIN_ID
        let fromId = Auth.auth().currentUser?.uid
        
        DataService.ds.admin.adjustMsgCount(addCount: true, fromId: fromId!)
        
        
        
        var dict: [String: Any] = ["toId": toId, "fromId": fromId, "timeStamp": timestamp, "fromUserName": DataService.ds.currentUser.name, "fromSeen": true, "toSeen": false, "itemType": selectedType, "itemUnit": "\(Int(self.stepperView.value))", "isItem": true,"imageUrl": item.urls["url0"] ] as [String : Any]
        
        
        
        
        for (key, element) in values {
            
            dict[key] = element
            print(dict)
        }
        // values.forEach({dict[$0] = $1 })
        //      firebaseMessage.updateChildValues(values)
        
        firebaseMessage.updateChildValues(dict, withCompletionBlock: {(error, ref) in
            
            if error != nil {
                print(error)
            }
        
            
            let userMsgRef = DataService.ds.REF_USER_MESSAGES.child(fromId!).child(toId)
            let msgId = firebaseMessage.key
            userMsgRef.updateChildValues([msgId: true])
            
            let recipientUserMsgRef = DataService.ds.REF_USER_MESSAGES.child(toId).child(fromId!)
            recipientUserMsgRef.updateChildValues([msgId: true])
            
            
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? ChatLogVC {
            
            destinationVC.user = DataService.ds.admin
//            destinationVC.item = self.item
//            destinationVC.itemType = self.selectedType
//            destinationVC.selectedUnits = Int(self.stepperView.value)
            
        }
    }
    

}

