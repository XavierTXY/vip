//
//  AddVC.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 30/7/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import UIKit
import OpalImagePicker
import TagListView

class CategoryCell: UITableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

class PhotoCell: UICollectionViewCell {
    @IBOutlet weak var itemImage: UIImageView!
    var addVC: AddVC!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

//        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageWasTapped))
//        itemImage.isUserInteractionEnabled = true
//        itemImage.addGestureRecognizer(tapGestureRecognizer)
    }
    
//    @objc func imageWasTapped() {
//        addVC.addImage()
//    }
}
class AddVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UITextFieldDelegate,TagListViewDelegate,OpalImagePickerControllerDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    //    @IBOutlet weak var tableView: UITableView!
    
    var item:Item!
    var updatedImg = false
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var descTextView: UITextView!
    @IBOutlet weak var typeTextField: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var conditionTextField: UITextField!
    @IBOutlet weak var brandTextField: UITextField!
    
    @IBOutlet weak var tagView: TagListView!
    
    var itemPicArray = [#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo")]
    var types = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()


        nameTextField.delegate = self
        typeTextField.delegate = self
        priceTextField.delegate = self
        categoryTextField.delegate = self
        conditionTextField.delegate = self
        
//        itemPicArray = [#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo")]
        tagView.textFont = UIFont.systemFont(ofSize: 17)
        tagView.alignment = .left
        tagView.delegate = self
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.scrollView.delegate = self
        
        if item != nil {
            nameTextField.text = item.name!
            priceTextField.text = item.price!
            categoryTextField.text = item.category!
            conditionTextField.text = item.condition!
            brandTextField.text = item.brand!
            
            descTextView.text = item.description!
           
            for type in self.item.typeArray {
                tagView.addTag(type)
                types.append(type)
            }
//            for (type,val) in item.types {
//                tagView.addTag(type)
//                types.append(type)
//                print("hihi \(type)")
//            }

            
        }
        
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelTapped))
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        print("aa")
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if typeTextField.isFirstResponder {
            
            if typeTextField.text != "" {
                types.append(typeTextField.text!)
                tagView.addTag(typeTextField.text!)
                typeTextField.text = ""
            }

            
        }
        return false
    }

    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        self.tagView.removeTag(title)
        
        var idx = 0
        for str in types {
            
            if str == title {
                types.remove(at: idx)
            }
            idx = idx + 1
        }
        
    }
    
    @objc func cancelTapped() {
        if self.item != nil {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height + 40
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }

    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return NO_PICTURE
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell

        if item != nil {

            if updatedImg {
                cell.itemImage.image = itemPicArray[indexPath.row]
            } else {
       
                if indexPath.row < item.urls.count {
                    cell.itemImage.loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: item.urls["url\(indexPath.row)"]!)
                    
                } else {
                    cell.itemImage.image = itemPicArray[indexPath.row]
                }
                itemPicArray.remove(at: indexPath.row)
                itemPicArray.append(cell.itemImage.image!)
            }

           
        } else {
            cell.itemImage.image = itemPicArray[indexPath.row]
        }

      
        
        cell.addVC = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        addImage()
    }
    
    @IBAction func listItem(_ sender: Any) {
        
//        print(nameTextField.text)
//        print(descTextView.text)
//        print(types)
//        print(priceTextField.text)
//        print(categoryTextField.text)
//        print(conditionTextField.text)
        
        var typeDict = [String: Bool]()
        
        for str in types {
            typeDict[str] = true
        }
        
        let item: Dictionary<String, AnyObject> = ["itemName": nameTextField.text! as AnyObject,"itemDesc": descTextView.text! as AnyObject,"itemPrice": priceTextField.text! as AnyObject,"itemCat": categoryTextField.text! as AnyObject,"itemCon": conditionTextField.text! as AnyObject,"itemTypes": typeDict as AnyObject, "itemBrand": brandTextField.text! as AnyObject]
        
//        if self.item != nil {
//
//            self.item.itemRef.updateChildValues(item)
//        } else {
        
            
            
        if self.item != nil {
//            for idx in 0...NO_PICTURE {
//                let cell = collectionView.cellForItem(at: IndexPath(row: idx, section: 0)) as! PhotoCell
//                itemPicArray.append(cell.itemImage.image!)
//            }
//            if updatedImg == false {
//                for idx in 0...NO_PICTURE {
//                    let cell = collectionView.cellForItem(at: IndexPath(row: idx, section: 0)) as! PhotoCell
//                    itemPicArray.append(cell.itemImage.image!)
//                }
//            }
            FirebaseService().uploadData(itemValue: item,images: itemPicArray, vc: self, ref: self.item.itemRef)
        } else {
            FirebaseService().uploadData(itemValue: item,images: itemPicArray, vc: self, ref: nil)
        }
        
//        }
    
        
    }
    
    func addImage() {
        let imagePicker = OpalImagePickerController()
        imagePicker.imagePickerDelegate = self
        imagePicker.maximumSelectionsAllowed = NO_PICTURE
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        //Cancel action?
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {

        //Save Images, update UI
//        itemPicArray = images
//        var startIdx = itemPicArray.count - images.count
        if images.count != 0 {
            itemPicArray = [#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo"),#imageLiteral(resourceName: "vipLogo")]
            //        itemPicArray.removeAll()
            for index in  0...images.count - 1  {
                itemPicArray[index] = images[index]
            }
            
            if images.count != NO_PICTURE {
                for index in images.count...NO_PICTURE - 1 {
                    itemPicArray[index] = #imageLiteral(resourceName: "vipLogo")
                }
            }
            
            updatedImg = true
        } else {

        }


        
        
        self.collectionView.reloadData()
        //Dismiss Controller
        presentedViewController?.dismiss(animated: true, completion: nil)
    }


}
