//
//  Extensions.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 24/8/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import Firebase

extension UIView {
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
}

extension UIImageView {
    
    
    func loadImageUsingCacheWithUrlStringWithoutIndex(imageUrl: String) {
        self.image = nil
        
        
        SDImageCache.shared().queryCacheOperation(forKey: (imageUrl as NSString) as String!, done: { (image2, data2, SDImageCacheType) in
            
            if( image2 != nil) {
                UIView.transition(with: self,
                                  duration:0.1,
                                  options: .transitionCrossDissolve,
                                  animations: {
                                    self.image = image2
                },
                                  completion: nil)
                
                print("it usese cached")
            } else {
                let ref = Storage.storage().reference(forURL: imageUrl)
                
                ref.getData(maxSize: 1 * 1024 * 1024, completion: { (data,error) in
                    if error != nil {
                        print("Xavier: unable to dowload image from fire")
                        self.image = #imageLiteral(resourceName: "ProfilePic")
                        print(error.debugDescription)
                    } else {
                        print("Xavier: image dowloaded from fire")
                        
                        // DispatchQueue.main.async(execute: { () -> Void in
                        if let imageData = data {
                            if let img = UIImage(data: imageData){
                                //
                                
                                //self.image = img
                                
                                DispatchQueue.main.async() {
                                    
                                    
                                    //                                    let updateCell = tv .cellForRow(at: index)
                                    //                                    if updateCell != nil {
                                    UIView.transition(with: self,
                                                      duration:0.1,
                                                      options: .transitionCrossDissolve,
                                                      animations: {
                                                        self.image = img
                                    },
                                                      completion: nil)
                                    
                                    
                                    
                                    // }
                                    
                                }
                                
                                // activityIndicatorView.stopAnimating()
                                // imageCache.setObject(img, forKey: imageUrl as NSString)
                                print("Xavier: cache saves img")
                                SDImageCache.shared().store(img, forKey: (imageUrl as NSString) as String!)
                                
                                
                                
                            }
                            
                        }
                        //  })
                        
                    }
                    
                })
            }
        })
    }
}
