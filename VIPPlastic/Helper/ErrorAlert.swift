//
//  ErrorAlert.swift
//  VIPPlastic
//
//  Created by XavierTanXY on 21/9/18.
//  Copyright © 2018 VIP Plastic Machinery. All rights reserved.
//

import Foundation
import UIKit
import PopupDialog

class ErrorAlert {
    
    func createAlert(title: String, msg: String, object: UIViewController) {
        //        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        //        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        //        object.present(alert, animated: true, completion: nil)
        
        // Prepare the popup assets
        
        let popup = PopupDialog(title: title, message: msg, image: nil)
        
        let buttonOne = CancelButton(title: "OK") {
            
        }
        
        popup.addButtons([buttonOne])
        
        // Present dialog
        object.present(popup, animated: true, completion: nil)
    }
    
    func show(title: String, msg: String, object: UIViewController) {
        // self.loaderStopAnimate()
        //Interaction().enableInteraction()
        
        
        
        let popup = PopupDialog(title: title, message: msg, image: nil)
        
        
        let buttonOne = CancelButton(title: "OK") {
            
        }
        
        popup.addButtons([buttonOne])
        
        // Present dialog
        object.present(popup, animated: true, completion: nil)
        
        //        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        //        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        //        object.present(alert, animated: true, completion: nil)
        // self.enableInteraction()
        
        
    }
}
